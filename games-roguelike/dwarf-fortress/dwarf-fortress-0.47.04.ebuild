# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

MY_PV=$(ver_rs 1- _ $(ver_cut 2-3))

DESCRIPTION="A single-player fantasy game"
HOMEPAGE="http://www.bay12games.com/dwarves/"
SRC_URI="amd64? ( http://www.bay12games.com/dwarves/df_${MY_PV}_linux.tar.bz2 )
	x86? ( http://www.bay12games.com/dwarves/df_${MY_PV}_linux32.tar.bz2 )"

LICENSE="free-noncomm BSD BitstreamVera"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug"
DEPEND="media-libs/glew:0
	media-libs/libsdl[X,joystick,video]
	media-libs/sdl-image[png]
	media-libs/sdl-ttf
	sys-libs/zlib
	virtual/glu
	x11-libs/gtk+:2
	media-libs/openal
	media-libs/libsndfile"

RDEPEND="${DEPEND}
	sys-libs/ncurses-compat:5[unicode]
	virtual/pkgconfig"

S=${WORKDIR}/df_linux
gamesdir="/opt/${PN}"
QA_PREBUILT="${gamesdir#/}/libs/Dwarf_Fortress"
RESTRICT="strip"

src_prepare() {
	( cd libs/ && find . ! -name 'Dwarf_Fortress' ! -name 'libgraphics.so' -type f -exec rm -f {} + ) || die
	sed -e "s:^gamesdir=.*:gamesdir=${gamesdir}:" "${FILESDIR}/dwarf-fortress" > dwarf-fortress || die
	default
}

src_install() {
	# install data-files and libs
	#insinto "${gamesdir}"
	insinto "/opt/dwarf-fortress"
	doins -r raw data libs
	# install our wrapper
	dobin dwarf-fortress
	dodoc README.linux *.txt
	fperms 755 "${gamesdir}"/libs/Dwarf_Fortress
}

pkg_postinst() {
	elog "System-wide Dwarf Fortress has been installed to ${gamesdir}. This is"
	elog "symlinked to ~/.dwarf-fortress when dwarf-fortress is run."
	elog "For more information on what exactly is replaced, see /usr/bin/dwarf-fortress."
	elog "Note: This means that the primary entry point is /usr/bin/dwarf-fortress."
	elog "Do not run ${gamesdir}/libs/Dwarf_Fortress."
	elog
	elog "Optional runtime dependencies:"
	elog "Install sys-libs/ncurses[unicode] for [PRINT_MODE:TEXT]"
	elog "Install media-libs/openal and media-libs/libsndfile for audio output"
	elog "Install media-libs/libsdl[opengl] for the OpenGL PRINT_MODE settings"
}
